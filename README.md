# CI report examples

This repository is used to populate data for features like:

- [Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/project/merge_requests/sast.html)
- [Dynamic Application Security Testing reports](https://docs.gitlab.com/ee/user/project/merge_requests/dast.html)
- [Container Scanning reports](https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html)
- [License management reports](https://docs.gitlab.com/ee/user/project/merge_requests/license_management.html)

It populates them by downloading test reports and exposing them via artifacts.

The pipeline also exposes some locally stored jUnit XML files as test reports, so that the test reporting functionality can be demoed.

## Usage

1. Run a pipeline
1. It will fail because of failed unit tests
1. Checkout its results and artifacts to get an overview of GitLab test reporting features.


## Development

The [`.gitlab-ci.yml`](.gitlab-ci.yml) file contains one job per scanner report and test suite.

Most of the jobs consist of downloading an up-to-date scanner report of the relevant type and using it as one of the
[`artifacts:reports`](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreports).
